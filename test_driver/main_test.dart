import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';
import 'package:ozzie/ozzie.dart';

void main() {
  FlutterDriver driver;
  Ozzie ozzie;

  setUpAll(() async {
    driver = await FlutterDriver.connect();
    ozzie = Ozzie.initWith(driver, groupName: 'counter');
  });

  tearDownAll(() async {
    // ignore: always_put_control_body_on_new_line
    if (driver != null) driver.close();
    ozzie.generateHtmlReport();
  });

  test('initial counter is 0', () async {
    await ozzie.profilePerformance('counter0', () async {
      await driver.waitFor(find.text('0'));
      await ozzie.takeScreenshot('initial_counter_is_0');
    });
  });

  test('initial counter is 0', () async {
    await ozzie.profilePerformance('counter1', () async {
      await driver.tap(find.byType('FloatingActionButton'));
      await driver.waitFor(find.text('1'));
      await ozzie.takeScreenshot('counter_is_1');
    });
  });
}